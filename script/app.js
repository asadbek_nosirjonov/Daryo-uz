window.addEventListener('DOMContentLoaded', () =>{

    const mainMenu = document.querySelector('.header__items');
    const openMenu = document.querySelector('.openMenu');
    const closeMenu = document.querySelector('.closeMenu');
    const scrollBtn = document.querySelector('.scrollToTop-btn')

        scrollBtn.addEventListener('click', () =>{
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
})

    openMenu.addEventListener('click', show);
    closeMenu.addEventListener('click', close);

function close() {
    mainMenu.style.left = '-100%';
}
function show() {
    mainMenu.style.display = 'flex';
    mainMenu.style.left = '0';
}

})